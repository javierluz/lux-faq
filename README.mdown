<h1>Descarga:</h1>
<ul>
<li>Puedes descargar el plugin desde mi <a href="https://github.com/javierluz/lux-faq" title="Github - luxFaq" target="_blank">github</a>.</li>
<li>También lo puedes obtener en el siguiente <a href="http://www.javierluz.cl/prontus_javierluz/site/artic/20120917/asocfile/20120917195128/lux_faq.zip" title="luxFaq.js" target="_blank">link de descarga</a>.</li>
</ul>
<h1>Estructura y código:</h1>
<h2>Head:</h2>
<p><span class="Apple-tab-span" style="white-space: pre;"> </span>Es necesario ingresar lo siguiente en el head del documento en donde se desea utilizar el plugin:</p>
<pre>&lt; link rel="stylesheet" type="text/css" media="screen" href="css/luxFaq.css" &gt;
&lt; script type="text/javascript" src="js/jquery.js" &gt;&lt; /script &gt;
&lt; script type="text/javascript" src="js/luxFaq.class.min.js" &gt;&lt; /script &gt;</pre>
<p><span class="Apple-tab-span" style="white-space: pre;"> </span>Claro que el css "luxFaq.css" es opcional, pues los estilos pueden ir incluidos en tu archivo de css personal, o –incluso– puedes hacer los estilos desde cero.</p>
<h2>Body:</h2>
<p>La estructura HTML necesaria para que el plugin funcione es la siguiente:</p>
<pre>&lt; ul class="luxFaq" id="luxFaq" &gt;
 &lt; li &gt;Título de pregunta &lt; div &gt;Respuesta&lt; /div &gt;&lt; /li &gt;
&lt; /ul &gt;</pre>
<p><span class="Apple-tab-span" style="white-space: pre;"> </span>Como se puede ver en la caja de arriba, basta con un ul con la clase luxFaq, y los li necesarios para cada pregunta. Dentro de estos li, debe existir un div... y hasta ahí es la estructura necesaria para el funcionamiento del plugin. Dentro de este div, eso si, podrás ingresar cualquier estructura que quieras.</p>
<p><span class="Apple-tab-span" style="white-space: pre;"> </span>Esa estructura interna será la respuesta, y se desplegará cuando un usuario haga click sobre la pregunta que lo contiene.</p>
<p>En estos div de respuesta pueden haber iframes, párrafos, links, imágenes, videos, más divs, etc...</p>
<h2>Estructura de ejemplo:</h2>
<p><span class="Apple-tab-span" style="white-space: pre;"> </span>La estructura utilizada en el ejemplo de más abajo (al final de este artículo) es la siguiente:</p>
<pre>&lt; ul class="luxFaq" id="luxFaq1" &gt;
 &lt; li &gt;Pregunta A &lt; div &gt;&lt; p &gt;Respuesta ejemplo 1&lt; /p &gt;&lt; /div &gt;&lt; /li &gt;
 &lt; li &gt;Pregunta B 
 &lt; div &gt;&lt; img src="/prontus_javierluz/site/artic/20120917/imag/foto_0000000120120917195128.jpg" alt="lion sunset" title="lion sunset" width="400" height="318" &gt;&lt; p &gt;Respuesta ejemplo 2&lt; /p &gt;&lt; /div &gt;&lt; /li &gt;
 &lt; li &gt;Pregunta C 
 &lt; div &gt;&lt; iframe width="420" height="315" src="http://www.youtube.com/embed/Pda4zULB3EA" frameborder="0" allowfullscreen &gt;&lt; /iframe &gt;
 &lt; p &gt;Respuesta ejemplo 3&lt; /p &gt;&lt; /div &gt;&lt; /li &gt;
 &lt; /ul &gt;</pre>
<h1>Extensión:</h1>
<p><span class="Apple-tab-span" style="white-space: pre;"> </span>El plugin tiene integradas varias características que pueden ser de utilidad a la hora de realizar FAQs un poco más complejos. A continuación, una tabla con los métodos disponibles, y para que sirven; así como un ejemplo de como aplicarlos en tu documento:</p>
<table cellpadding="20" cellspacing="20">
<tbody>
<tr><th>Método</th><th>Funcionamiento</th></tr>
<tr>
<td>luxFaq.velocidad</td>
<td>Cambia la velocidad a la que se animan los cambios de estado de las preguntas. Los valores admitidos son los mismos que permite jQuery por defecto (eje: slow; fast; normal; [int])</td>
</tr>
<tr>
<td>luxFaq.divActivo</td>
<td>Al hacer click sobre una respuesta, en vez de que no pase nada, la pregunta se contrae, como si se hubiese clickeado sobre el título (funcionamiento normal). Por defecto es false, pero puede cambiarse a true.</td>
</tr>
<tr>
<td>luxFaq.stopYoutube</td>
<td>Al cambiar de pregunta, si esta contiene un video de youtube, este se detiene automáticamente. Por defecto es false, pero puede cambiarse a true.</td>
</tr>
<tr>
<td>luxFaq.miniaturas</td>
<td>Si está en true, las imágenes dentro de la respuesta disminuirán de tamaño, y al darles click, volverán a su tamaño original. Por defecto está en false.</td>
</tr>
<tr>
<td>luxFaq.miniaturasConMouseOver</td>
<td>En vez de necesitar un click, basta con pasar el mouse sobre una miniatura para verla a tamaño real. <strong>[Requiere que luxFaq.miniaturas esté activado, es decir, en true]</strong></td>
</tr>
<tr>
<td>luxFaq.autocolapse</td>
<td>Si está en false, las preguntas se mantendrán abiertas, incluso si se selecciona otra pregunta. Para cerrarlas, se necesitará que el usuario clickee nuevamente el titular de la pregunta abierta. Por defecto está en false, es decir que cuando se clickee una pregunta, las que estén abiertas se cerrarán automáticamente.</td>
</tr>
<tr>
<td>luxFaq.preguntaAbiertaDesdeElInicio</td>
<td>Si está en true, se activa la posibilidad de que, cuando se inicie el script, haya una pregunta abierta de inmediato. Por defecto, este valor es false (por lo que ninguna pregunta aparece abierta desde el inicio).</td>
</tr>
<tr>
<td>luxFaq.numeroPreguntaAbiertaDesdeElInicio</td>
<td>Elegimos que pregunta aparecerá abierta inmediatamente al iniciar el script. Por defecto es 0 (la primera pregunta). Este método solo acepta [int], es decir, números completos (no acepta decimales). <strong>[Requiere que luxFaq.preguntaAbiertaDesdeElInicio esté con un valor true].</strong></td>
</tr>
</tbody>
</table>
<p>Para aplicar estos métodos de extensión, basta con incluir esto en el header, y modificarlo a gusto:</p>
<pre>&lt; script type="text/javascript" &gt; 
$(function($) { 
 luxFaq.velocidad='slow'; 
 luxFaq.divActivo=true; 
 luxFaq.stopYoutube=true; 
 luxFaq.miniaturas=true; 
 luxFaq.miniaturasConMouseOver=true; 
 luxFaq.autocolapse=false; 
 luxFaq.preguntaAbiertaDesdeElInicio=true; 
 luxFaq.numeroPreguntaAbiertaDesdeElInicio=0; 
}); 
&lt; /script &gt;</pre>
<h1>Ejemplo de funcionamiento</h1>
<p><a href="http://www.javierluz.cl/prontus_javierluz/site/artic/20120917/pags/20120917195128.html" target="_blanc">http://www.javierluz.cl/prontus_javierluz/site/artic/20120917/pags/20120917195128.html</a></p>