/* JavaScript slideshow jQuery Plug-in.
Nombre: luxFaq
Versión: 0.6
Autor: Javier Luz
Web autor: http://www.javierluz.cl

(!)IMPORTANTE: Este plugin es totalmente gratis. Podrás utilizarlo como se te plasca, 
mas lo único que te pido, es que mantengas los créditos dentro de este archivo 
(en la parte superior). Y, si puedes, linkees a mi web, dando a entender 
que soy el autor original */

/* ------------------- Instrucciones -------------------
Puedes cambiar las propiedades de este javascript 
ingresando un nuevo script en el head de tu documento. Eje:

luxFaq.velocidad='slow'; //cambia la velocidad a "slow".
luxFaq.divActivo=true; // Al hacer click sobre una respuesta, en vez de que no pase nada, la pregunta se contrae.
luxFaq.stopYoutube=true; // Al cambiar de pregunta, si esta contiene un video de youtube, este se detiene [por defecto: false].
luxFaq.miniaturas=false; // Si está en true, las imágenes dentro de la respuesta disminuirán de tamaño, y al darles click, volverán a su tamaño original.
luxFaq.miniaturasConMouseOver=false; // En vez de necesitar un click, basta con pasar el mouse sobre un thumbnail para verla a tamaño real (requiere que miniaturas esté activado).
luxFaq.autocolapse:true // Si está en false, las preguntas se mantendrán abiertas, incluso si se selecciona otra pregunta. Para cerrarlas, se necesitará que el usuario clickee nuevamente el titular de la pregunta abierta.
luxFaq.preguntaAbiertaDesdeElInicio=true; //Si está en true, se activa la posibilidad de que (al iniciar el script) haya una pregunta abierta de inmediato (por defecto, 0, es decir, la primera pregunta).
luxFaq.numeroPreguntaAbiertaDesdeElInicio=3; //Elegimos que pregunta aparecerá abierta inmediatamente al iniciar el script. Por defecto es 0 (primera pregunta).

FIN Instrucciones */

$(window).load(function() {luxFaq.init();});
//Si por alguna razón, el plugin no se inicializa, copia esto dentro de la etiqueta body de tu documento-> onload="luxFaq.init();"

var luxFaq = {
	/*-------- Propiedades Públicas --------*/
	velocidad: 'fast',
	divActivo: false,
	stopYoutube:false,
	miniaturas:false,
	miniaturasConMouseOver:false,
	autocolapse:true,
	preguntaAbiertaDesdeElInicio:false,
	numeroPreguntaAbiertaDesdeElInicio:0,
	/* FIN Propiedades Públicas */
	
	// función init (inicializa todos los procesos del plugin)
	init: function() {
		// Cada vez que se hace click sobre una pregunta, se llama a luxFaq.trigger();
		var luxFaqSelector = $('.luxFaq');
		
		luxFaqSelector.children('li').bind('click', luxFaq.trigger);
		luxFaq.generarIdsUnicas();
		if(luxFaq.preguntaAbiertaDesdeElInicio) {
			$('.luxFaq #preg-'+luxFaq.numeroPreguntaAbiertaDesdeElInicio).children('div').css('display','block');
		}
		// inicializar divActivo
		if(!luxFaq.divActivo) { luxFaqSelector.children('li').children('div').bind('click', function() {return false;}); }
		// inicializar miniaturas
		if(luxFaq.miniaturas) { luxFaq.activarMiniaturas(); }
	},
	// Método utilizado para mostrar u ocultar las respuestas.
	trigger: function(obj) {
		if(luxFaq.stopYoutube) {
			luxFaq.stopYoutubeFunc();
		}
		if(luxFaq.autocolapse) {
			$(this).siblings('li').children('div').slideUp(luxFaq.velocidad);
		}
		$(this).children('div').slideToggle(luxFaq.velocidad);
	},
	stopYoutubeFunc: function() {
		$('iframe[src^="http://www.youtube.com"]').each(function () {
			var src = jQuery(this).attr("src");
			jQuery(this).attr("src", "");
			jQuery(this).attr("src", src);
    });
  },
	
	// Si las miniaturas están en true, se achican todas las imágenes de las respuestas, y se llaman los métodos correspondientes.
	activarMiniaturas: function() {
	  var imagenesRequeridas = $('.luxFaq').children('li').children('div').children('img');
	  imagenesRequeridas.removeAttr('width'); imagenesRequeridas.removeAttr('height'); //removemos los atributos width y height (para que no interrumpan el css)
		luxFaq.maxSizeMiniaturas(); //llamamos al método que define los tamaños (más abajo).
		if(!luxFaq.miniaturasConMouseOver) { // Sin mouseover, es decir, requiere un click
		imagenesRequeridas.bind('click',function() {
			$(this).css({'width':'auto','height':'auto'});
		});
		} else { //Sino, con pasar el mouse sobre la imagen, esta se agrandará, y viceversa.
		imagenesRequeridas.bind('mouseenter',function() {
			$(this).css({'width':'auto','height':'auto'});
		});
		imagenesRequeridas.bind('mouseleave',function() {
			luxFaq.maxSizeMiniaturas();
		});
	  }
	},
	// Definimos el máximo de tamaño de una imagen.
	maxSizeMiniaturas: function() {
		var max_size = '100';
		$(".luxFaq > li > div img").each(function(i) {
			if ($(this).height() > $(this).width()) {
				var h = max_size;
				var w = Math.ceil($(this).width() / $(this).height() * max_size);
			} else {
				var w = max_size;
				var h = Math.ceil($(this).height() / $(this).width() * max_size);
			}
			$(this).css({ 'height': h+'px', 'width': w+'px' });
		});
	},
	generarIdsUnicas: function() {
		$('.luxFaq li').each(function(index) {
			$(this).attr({
				'id': 'preg-' + index
			});
		});
	}
};